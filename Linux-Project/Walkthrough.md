<table align="center"><tr><td align="center" width="9999">
<img src="https://www.fbi.gov/image-repository/cyber-crime.jpg/@@images/image/large" align="center" width="500" alt="Project icon">


# Linux Project Walkthrough

</td></tr></table>


## Table of contents

- [Linux project](#linux-project-walkthrough)
	- [Table of contents](#table-of-contents)
	- [Over View](#over-view)
	- [Walkthrough](#walkthrough)
	- [Creator](#creator)

## Over View 

We got a machine and we were asked:

1. Get into the machine (Rafi account) without pass.

 	2.  Find out how myfs works and get the password for the HR program which stores in password.bits "block device".
 	3. Find out what files does the HR program use.

Here are the steps I took to do this:

* Checked out how to recover account password in Linux OS.
* Reverse Engineered myfs and unlock the root directory and passwords directory
* Used some program which used strace for finding out what files does the HR program use.



## Walkthrough

So, first I opened YouTube and searched: "How to recover Ubuntu Linux password"

Linux approach for security is that if you physical access to the machine you can do whatever you want so it was easy.

I Just followed the steps in the [video](https://youtu.be/yqL9IIWT2dw) which are:

* ![1](Screenshots/aaa.jpeg)
* ![2](Screenshots/222.jpeg)
* ![3](Screenshots/333.jpeg)

* And then, type to following:

  mount -rw -o remount /

  passwd rafii - choose new password and exit from the root shell.

That's it.

Finished with the first stage, continuing to the second stage of the project.

I run the file system program and gave it the password.bits as a "block device".

 Then, I tried to see the content of some file but the directory seems to be locked.

![using my fs](Screenshots/using_myfs.jpeg)

OK so we need somehow to get the content of some file that contains the password (probably in passwords directory)

For this task there are three approaches 

1. Understand how the file system stores the data and find the correct password in password.bits (Data Reverse Engineering)
2. Using strings command and finish the project
3. Reverse  myfs program, find out how it checks for lock directory, unlock the directories and use myfs for discover the password 

I started from the second option and after it worked I realized that it isn't a valid way and it misses the main point of the project, so I moved to the third option.

First I searched where does myfs program locate in the machine 

![which myfs](Screenshots/which_myfs.jpeg)

we can simply see that it is a 64bit elf (which written in c++) with the file command

![file myfs](Screenshots/file_myfs.jpeg)

We can clearly see that the creators of the project try to made it easy for us - they didn't applied any obfuscated (they even included debug_info !).



Then, I copy myfs program to my machine and open it in IDA.

We need to search for the String "Locked directory, cannot pass" and then we probably going to see the check for the locking.

So go to View -> Open subview -> Strings then search for the string.

![Locked directory string in IDA](Screenshots/locked_dir_str.jpeg)	

Then, double click and press x for xrefs

![xref locked dir str](Screenshots/locked_sir_str2.jpeg)

And here is the checking for lock directory: 

![lock DIR check](Screenshots/lock_dir_check.jpeg)

OK. There is some structure in rbp+te, its address moved to rax (64 bits address because of 64 bit machine) and there is probably integer in rax+0xc that if it's second bits turn on the directory is lock.

But what structure is located in rbp+te ??

We need to find the function that set its content.

![rbp+te](Screenshots/rbp+te.jpeg)

index_to_addr function simply multiplies the integer PARAM in 16 and adds to it 5

![index to address](Screenshots/index_to_addr.jpeg)

So we can understand that the size of each structure is 16 bytes and the structures start from offset 5 in the "block device".

Then the function read set the content of rbp+te according the given address 

We can also see the type of te variable which is MyFs::table_entry; 

![te](Screenshots/te_strcuture.jpeg)

In the structure window in IDA we can see all the details about the table_entry structure

![table entry](Screenshots/table_entry.jpeg)

OK. and as we assume in + 0xc there is integer which attend to be flags and it its &2 result is positive the directory is lock.

This is what we got so far. There is structure which called table_entry it is used for both directories and files , it's size is 16 bytes (4 byte address, 4 bytes size, 4 bytes valid and 4 bytes flags) and all the structures are located in the "block device" from offset 5.

flags is the one which important for us - if the structure contain detailed about directory the result of bitwise and on the flags and 1 will be true and if the the directory is locked the result of bitwise and with flags and 2 should be true.

Lets move to hex view of password.bits and see all that information.

We can see the first table_entry which aontains the deatils of the root directory

![root dir](Screenshots/root_DIR.jpg)

We can see that all the values are represented in little endian so we need to change the order for understand the data correctly.  

Before;

"DDF20600" - address

"9F550000" - size

"01074A31" - valid 

"03000000" - flags

After:

0x0006f2dd - address

0x0000559f - size

0x314a0701 - valid

0x3 - flags

Lets go to the address and see what's there

![move](Screenshots/move.jpeg)

We can see the names of all the files.

So the first structure is the root directory and it contains all the files or directories in it.

The flags of the root directory are 3 which means is directory (3 & 1 > 0) and also locked (3 & 2 > 0)

So all what we needs to do to unlock the root directory is to change the flags value for odd number that it's second bit is off like 1,  5, 9, 13 etc.

![change flags value](Screenshots/change.jpeg)

Now we can see the content of each file. 

![ls root](Screenshots/myfs.jpeg)

Notice that the directory passwords is also kind of file (which contains the names of all the files) so we can see its content (the files in it) 

![ls passwords DIR](Screenshots/ls_pass.jpeg)

![lock pass](Screenshots/lock_pass.jpeg)

Though passwords directory is still lock so we need to change it's flags value either, It's the third structure from the beginning (root directory, README and passwords directory)

![pass DIR flags](Screenshots/pass_DIR_flags.jpeg)

Now we can see the password and we finished the second stage.

![pass](Screenshots/pass.jpeg)

Moving on.

We got the password for the HR program and we need to check in wich files doed that program using.

Also for this problem there are three approaches:

1.  Using tools like strace to see the syscalls (open/read file will be in there and also it's path)
2. strings command 
3. Reverse Engineering the program 



I tried all of them and I'll show the way of each one.

First I tried to find some tool to show me only the files the program use instead of all the syscalls  also those who are not relevant to open/read files.

I find this tool:  [tracefile](https://gitlab.com/ole.tange/tangetools/-/tree/master/tracefile)

and the output:

![tracefile res](Screenshots/res.jpeg)

 using string command: strings /usr/bin/prog

![strings](Screenshots/strings.jpeg)

Open prog in IDA:



![data1](Screenshots/data1.jpeg)

![data2](Screenshots/data2.jpeg)

So finally, those are all the files that the program using:

````reStructuredText
/var/data/users/Onalorks/settings
/var/data/users/Shayceeder/settings
/var/data/users/TearSimple/settings
/var/data/users/Theborgia/settings
/var/data/users/Chromereve/settings
/var/data/users/TheborgVander/settings
/var/data/users/Earlyzena/settings
/var/data/users/Expertnett/settings
/var/data/users/CoopsKissez/settings
/var/data/users/Ditymato/settings
/var/data/users/Kondravn/settings
/var/data/users/Sarpluti/settings
/var/data/users/Fratechin/settings
/var/data/users/Slimixiate/settings
/var/data/users/Polytomun/settings
/var/data/users/Liverens/settings
/var/data/users/BenComments/settings
/var/data/users/Toysrunds/settings
/var/data/users/Frogrintar/settings
/var/data/users/ThegaRaven/settings
/var/data/users/Newgamicr/settings
/var/data/users/GreeWicked/settings
/var/data/users/Moonlilife/settings
/var/data/users/Penaste/settings
/var/data/users/DarthDays/settings
/var/data/users/Kamaxia/settings
/var/data/users/Breakingan/settings
/var/data/users/Jewelettena/settings
/var/data/users/Equolexy/settings
/var/data/users/Reprevora/settings
/var/data/users/KittyBe/settings
/var/data/users/Bullerry/settings
/var/data/users/Stoneures/settings
/var/data/users/LandSuave/settings
/var/data/users/Danny/settings
/var/data/users/Gobaryon/settings
/var/data/users/AmyBomberTales/settings
/var/data/users/Kinglyrion/settings
/var/data/users/SaiyanWellStyx/settings
/var/data/users/Dynafor/settings
/var/data/users/FlyDiscover/settings
/var/data/users/MonReportPerson/settings
/var/data/users/Jillast/settings
/var/data/users/Bloggerty/settings
/var/data/users/Talkfulsi/settings
/var/data/users/Framoeng/settings
/var/data/users/Tumindus/settings
/var/data/users/Stonetiongo/settings
/var/data/users/Citywelt/settings
/var/data/users/Agilland/settings
/var/data/users/Banditsxml/settings
/var/data/users/Centecmat/settings
/var/data/users/Bighemati/settings
/var/data/users/LabsMatrix/settings
/var/data/users/Mapsohn/settings
/var/data/users/Teereder/settings
/var/data/users/Topicheaddp/settings
/var/data/users/EdgyIntcatForever/settings
/var/data/users/Sonsptia/settings
/var/data/users/Cadalent/settings
/var/data/users/SnoMyheroBing/settings
/var/data/users/SkateClownDude/settings
/var/data/users/Horraytech/settings
/var/data/users/Cornycast/settings
/var/data/users/Freezinger/settings
/var/data/users/Meatelle/settings
/var/data/users/Medictra/settings
/var/data/users/Bauerrami/settings
/var/data/users/Dudersonta/settings
/var/data/users/CakeTale/settings
/var/data/users/Citintall/settings
/var/data/users/Animetelf/settings
/var/data/users/Jiggynyan/settings
/var/data/users/FairyJean/settings
/var/data/users/Motelon/settings
/var/data/users/Briduake/settings
/var/data/users/SpinPrideGolden/settings
/var/data/users/HearFinestDirty/settings
/var/data/users/SarenExpert/settings
/var/data/users/RunningDogg/settings
/var/data/users/Casualthur/settings
/var/data/users/Darigollia/settings
/var/data/users/Initsxip/settings
/var/data/users/BlinkForlife/settings
/var/data/users/NotHipGal/settings
/var/data/users/IdolGalDubya/settings
/var/data/users/TrainTin/settings
/var/data/users/SoCrunch/settings
/var/data/users/Eonerema/settings
/var/data/users/NozyTown/settings
/var/data/users/Coopshighpe/settings
/var/data/users/Mobindru/settings
/var/data/users/RocketCrash/settings
/var/data/users/ReeBeeKissez/settings
/var/data/users/FarerNote/settings
/var/data/users/Shcamirid/settings
/var/data/users/Roaricahi/settings
/var/data/users/ReporterChi/settings
/var/data/users/Gidon/passwd
/var/data/users/Onalorks/passwd
/var/data/users/Shayceeder/passwd
/var/data/users/TearSimple/passwd
/var/data/users/Theborgia/passwd
/var/data/users/Chromereve/passwd
/var/data/users/TheborgVander/passwd
/var/data/users/Earlyzena/passwd
/var/data/users/Expertnett/passwd
/var/data/users/CoopsKissez/passwd
/var/data/users/Ditymato/passwd
/var/data/users/Kondravn/passwd
/var/data/users/Sarpluti/passwd
/var/data/users/Fratechin/passwd
/var/data/users/Slimixiate/passwd
/var/data/users/Polytomun/passwd
/var/data/users/Liverens/passwd
/var/data/users/BenComments/passwd
/var/data/users/Toysrunds/passwd
/var/data/users/Frogrintar/passwd
/var/data/users/ThegaRaven/passwd
/var/data/users/Newgamicr/passwd
/var/data/users/GreeWicked/passwd
/var/data/users/Moonlilife/passwd
/var/data/users/Penaste/passwd
/var/data/users/DarthDays/passwd
/var/data/users/Kamaxia/passwd
/var/data/users/Breakingan/passwd
/var/data/users/Jewelettena/passwd
/var/data/users/Equolexy/passwd
/var/data/users/Reprevora/passwd
/var/data/users/KittyBe/passwd
/var/data/users/Bullerry/passwd
/var/data/users/Stoneures/passwd
/var/data/users/LandSuave/passwd
/var/data/users/Danny/passwd
/var/data/users/Gobaryon/passwd
/var/data/users/AmyBomberTales/passwd
/var/data/users/Kinglyrion/passwd
/var/data/users/SaiyanWellStyx/passwd
/var/data/users/Dynafor/passwd
/var/data/users/FlyDiscover/passwd
/var/data/users/MonReportPerson/passwd
/var/data/users/Jillast/passwd
/var/data/users/Bloggerty/passwd
/var/data/users/Talkfulsi/passwd
/var/data/users/Framoeng/passwd
/var/data/users/Tumindus/passwd
/var/data/users/Stonetiongo/passwd
/var/data/users/Citywelt/passwd
/var/data/users/Agilland/passwd
/var/data/users/Banditsxml/passwd
/var/data/users/Centecmat/passwd
/var/data/users/Bighemati/passwd
/var/data/users/LabsMatrix/passwd
/var/data/users/Mapsohn/passwd
/var/data/users/Teereder/passwd
/var/data/users/Topicheaddp/passwd
/var/data/users/EdgyIntcatForever/passwd
/var/data/users/Sonsptia/passwd
/var/data/users/Cadalent/passwd
/var/data/users/SnoMyheroBing/passwd
/var/data/users/SkateClownDude/passwd
/var/data/users/Horraytech/passwd
/var/data/users/Cornycast/passwd
/var/data/users/Freezinger/passwd
/var/data/users/Meatelle/passwd
/var/data/users/Medictra/passwd
/var/data/users/Bauerrami/passwd
/var/data/users/Dudersonta/passwd
/var/data/users/CakeTale/passwd
/var/data/users/Citintall/passwd
/var/data/users/Animetelf/passwd
/var/data/users/Jiggynyan/passwd
/var/data/users/FairyJean/passwd
/var/data/users/Motelon/passwd
/var/data/users/Briduake/passwd
/var/data/users/SpinPrideGolden/passwd
/var/data/users/HearFinestDirty/passwd
/var/data/users/SarenExpert/passwd
/var/data/users/RunningDogg/passwd
/var/data/users/Casualthur/passwd
/var/data/users/Darigollia/passwd
/var/data/users/Initsxip/passwd
/var/data/users/BlinkForlife/passwd
/var/data/users/NotHipGal/passwd
/var/data/users/IdolGalDubya/passwd
/var/data/users/TrainTin/passwd
/var/data/users/SoCrunch/passwd
/var/data/users/Eonerema/passwd
/var/data/users/NozyTown/passwd
/var/data/users/Coopshighpe/passwd
/var/data/users/Mobindru/passwd
/var/data/users/RocketCrash/passwd
/var/data/users/ReeBeeKissez/passwd
/var/data/users/FarerNote/passwd
/var/data/users/Shcamirid/passwd
/var/data/users/Roaricahi/passwd
/var/data/users/ReporterChi/passwd
````

Their content are in files_content.md in the repo.

Tnx for reading.



## Creator

**Ariel Adjadj**

- <https://gitlab.com/arieladjadj>
- <https://github.com/arieladjadj>
